# A Demo for AJAX in GoLang

This demo shows how to communicate with backend through AJAX.

# Prerequisite

You need to [install GoLang](https://golang.org/dl/) to run the demo.

# Run

Simply type:

```
make
```
