package main

import (
	"crypto/md5"
	"flag"
	"fmt"
	"html/template"
	"log"
	"net/http"
)

func main() {
	port := flag.Int("p", 3001, "port")
	flag.Parse()

	// index
	http.HandleFunc("/", index)

	// md5
	http.HandleFunc("/md5/", md5Hash)

	// Start
	fmt.Printf("Listenning on :%d\n", *port)
	if err := http.ListenAndServe(fmt.Sprintf(":%d", *port), nil); err != nil {
		log.Fatal(err)
	}
}

// index

var indexTemplate = template.Must(template.ParseFiles("index.html"))

func index(w http.ResponseWriter, r *http.Request) {
	indexTemplate.Execute(w, nil)
}

// md5

func md5Hash(w http.ResponseWriter, r *http.Request) {
	text := r.URL.Query().Get("text")
	data := []byte(text)
	hash := fmt.Sprintf("%x", md5.Sum(data))
	w.Write([]byte(hash))
}
